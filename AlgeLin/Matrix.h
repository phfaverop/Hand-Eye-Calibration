//
//  Matrix.h
//  Hand_Eye_Calibration
//
//  Created by Paulo Favero Pereira on 9/4/19.
//  Copyright � 2019 Paulo Favero Pereira. All rights reserved.
//
#pragma once

#include "Vector3.h"

//Row Major Convention
class Matrix3x3 {
public:

	Matrix3x3();
	Matrix3x3(float v11, float v12, float v13,
		float v21, float v22, float v23,
		float v31, float v32, float v33);
	Matrix3x3(Vector3 rotAxis, float angle);

	/**
	 Creates a simetric skew matrix from a vector
	 @param rotAxis Rotational axis
	 @return Simetric skew matrix
	 */
	Matrix3x3 Skew(Vector3 rotAxis);
	void MultScalar(float scalar);
	Vector3 MultFront(Vector3 vec);
	Vector3 MultBack(Vector3 vec);
	Matrix3x3 MultFront(Matrix3x3 mat);
	Matrix3x3 MultBack(Matrix3x3 mat);
	void Transpose();
	Matrix3x3 GetTranspose();
	Matrix3x3 GetCopy();
	void SetZero();


	bool IsSimetricSkew();
	bool Equals(Matrix3x3 matB);
	bool IsZero();
	bool IsIdentity();
	float Trace();
	Vector3 GetRotationalAxis();

	Matrix3x3 operator * (float scalar);
	Matrix3x3 operator * (Matrix3x3 matB);
	Matrix3x3 operator + (Matrix3x3 matB);
	Matrix3x3 operator + (float scalar);
	Matrix3x3 operator - (Matrix3x3 matB);
	Matrix3x3 operator - (float scalar);
	void operator = (Matrix3x3 matB);

	void ToString();

	float m11, m12, m13, m21, m22, m23, m31, m32, m33;
};