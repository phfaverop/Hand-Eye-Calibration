//
//  AlgeLin.h
//  Hand_Eye_Calibration
//
//  Created by Paulo Favero Pereira on 9/3/19.
//  Copyright � 2019 Paulo Favero Pereira. All rights reserved.
//

#pragma once

#include "Pose34.h"
#include "Point3D.h"

#define PI = 3.14159265f;
#define _RAD(degree) (PI * degree / 180.0f)