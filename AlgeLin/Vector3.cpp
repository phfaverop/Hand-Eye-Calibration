//
//  Vector3.cpp
//  Hand_Eye_Calibration
//
//  Created by Paulo Favero Pereira on 9/4/19.
//  Copyright � 2019 Paulo Favero Pereira. All rights reserved.
//

#include "Vector3.h"

///////////////////////////////////////////////////////////////////////////
///////////////                Vector Class                 ///////////////
///////////////////////////////////////////////////////////////////////////

Vector3::Vector3()
{
	mX = 0.0;
	mY = 0.0;
	mZ = 0.0;
}

Vector3::Vector3(float x, float y, float z)
{
	mX = x;
	mY = y;
	mZ = z;
}

Vector3::Vector3(const Vector3& vecOrig)
{
	mX = vecOrig.mX;
	mY = vecOrig.mY;
	mZ = vecOrig.mZ;
}


Vector3 Vector3::operator * (float scalar)
{
	Vector3 vecRes;
	vecRes.mX = mX * scalar; vecRes.mY = mY * scalar; vecRes.mZ = mZ * scalar;
	return vecRes;
}


float Vector3::operator * (Vector3 vecB)
{
	float res;
	res = mX * vecB.mX + mY * vecB.mY + mZ * vecB.mZ;
	return res;
}

bool Vector3::operator == (Vector3 vecB)
{
	return ((abs(mX - vecB.mX) <= FLT_EPSILON) && (abs(mY - vecB.mY) <= FLT_EPSILON) && (abs(mZ - vecB.mZ) <= FLT_EPSILON));
}

//Pr = 2 sin(theta / 2) * [nl n2 n3]

Vector3::Vector3(Vector3 rotAxis, float angle)
{
	//    rotAxis.Normalize();
	mX = (2 * sin(angle / 2)) * rotAxis.mX;
	mY = (2 * sin(angle / 2)) * rotAxis.mY;
	mZ = (2 * sin(angle / 2)) * rotAxis.mZ;
	Normalize();
}


float Vector3::Magnitude()
{
	return (pow(mX, 2) + pow(mY, 2) + pow(mZ, 2));
}


float Vector3::Norm()
{
	return sqrt(Magnitude());
}


void Vector3::Normalize()
{
	float norm = Norm();
	mX /= norm; mY /= norm; mZ /= norm;
}


void Vector3::MultScalar(float scalar)
{
	mX *= scalar; mY *= scalar; mZ *= scalar;
}

Vector3 Vector3::GetReverse()
{
	return Vector3(-mX, -mY, -mZ);
}


float Vector3::MultVector(Vector3 vecTrans)
{
	return mX * vecTrans.mX + mY * vecTrans.mY + mZ * vecTrans.mZ;
}

Vector3 Vector3::OuterProduct(Vector3 vecB)
{
	Vector3 vecRes;
	vecRes.mX = mX * (vecB.mX + vecB.mY + vecB.mZ);
	vecRes.mY = mY * (vecB.mX + vecB.mY + vecB.mZ);
	vecRes.mZ = mZ * (vecB.mX + vecB.mY + vecB.mZ);
	return vecRes;
}

Vector3 Vector3::CrossProduct(Vector3 vecB)
{
	Vector3 vecRes;
	vecRes.mX = mY * vecB.mZ - mZ * vecB.mY;
	vecRes.mY = mZ * vecB.mX - mX * vecB.mZ;
	vecRes.mZ = mX * vecB.mY - mY * vecB.mX;
	return vecRes;
}

float Vector3::DotProduct(Vector3 vecB)
{
	return mX * vecB.mX + mY * vecB.mY + mZ * vecB.mZ;
}

bool Vector3::IsEqual(Vector3 vecB)
{
	if ((mX - vecB.mX) > FLT_EPSILON) {
		return false;
	}
	if ((mY - vecB.mY) > FLT_EPSILON) {
		return false;
	}
	if ((mZ - vecB.mZ) > FLT_EPSILON) {
		return false;
	}
	return true;
}

float Vector3::Alpha() {

	return sqrt(4 - Magnitude());
}

void Vector3::ToString()
{
	std::cout << "Vector \n( " << mX << ", " << mY << ", " << mZ << " )" << std::endl;
}
