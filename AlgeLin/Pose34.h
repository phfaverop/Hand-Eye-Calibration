//
//  Pose34.h
//  Hand_Eye_Calibration
//
//  Created by Paulo Favero Pereira on 9/3/19.
//  Copyright � 2019 Paulo Favero Pereira. All rights reserved.
//

#pragma once
#include "Matrix.h"

class Pose34 {
public:
	Pose34();
	Pose34(Matrix3x3 matRot, Vector3 vecOff);
	Pose34 Inverse();
	bool IsIdentity();

	void ToString();

	Matrix3x3 m_matRot;
	Vector3 m_vecOff;
};