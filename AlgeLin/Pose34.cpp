//
//  Pose34.cpp
//  Hand_Eye_Calibration
//
//  Created by Paulo Favero Pereira on 9/3/19.
//  Copyright � 2019 Paulo Favero Pereira. All rights reserved.
//
#pragma once
#include "Pose34.h"

///////////////////////////////////////////////////////////////////////////
///////////////                Pose34 Class                 ///////////////
///////////////////////////////////////////////////////////////////////////

Pose34::Pose34()
{
	m_matRot = Matrix3x3();
	//m_vecOff = Vector3();
}

Pose34::Pose34(Matrix3x3 matRot, Vector3 vecOff)
{
	m_matRot = matRot;
	//m_vecOff = vecOff;
}

Pose34 Pose34::Inverse()
{
	return Pose34(m_matRot.GetTranspose(), m_matRot.GetTranspose().MultFront(m_vecOff.GetReverse()));
}

bool Pose34::IsIdentity()
{
	if (!m_matRot.IsIdentity())
	{
		return false;
	}
	if (!m_vecOff.IsEqual(Vector3()))
	{
		return false;
	}
	return true;
}

void Pose34::ToString()
{
	std::cout << "Pose \n( " << m_matRot.m11 << ", " << m_matRot.m12 << ", " << m_matRot.m13 << ", " << m_vecOff.mX << "," << std::endl;
	std::cout << m_matRot.m21 << ", " << m_matRot.m22 << ", " << m_matRot.m23 << ", " << m_vecOff.mY << "," << std::endl;
	std::cout << m_matRot.m31 << ", " << m_matRot.m32 << ", " << m_matRot.m33 << ", " << m_vecOff.mZ << ")" << std::endl;
}