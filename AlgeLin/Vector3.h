#pragma once
//
//  Vector3.hpp
//  Hand_Eye_Calibration
//
//  Created by Paulo Favero Pereira on 9/4/19.
//  Copyright � 2019 Paulo Favero Pereira. All rights reserved.
//

#ifndef Vector3_h
#define Vector3_h

#include <stdio.h>
#include <iostream>
#include <math.h>


class Vector3 {
public:
	Vector3();
	Vector3(float x, float y, float  z);
	Vector3(Vector3 vec, float angle); //Pr = 2 sin(theta / 2) * [nl n2 n3]
	Vector3(const Vector3& vecOrig);

	/**
	 Vector norm |vec|
	 @return |vec\
	 */
	float Norm();
	/**
	 Vector Magnitude |vec|�2

	 @return |vec|�2
	 */
	float Magnitude();
	void Normalize();
	void MultScalar(float scalar);
	Vector3 GetReverse();
	float MultVector(Vector3 vecTrans);
	Vector3 OuterProduct(Vector3 vecB);
	Vector3 CrossProduct(Vector3 vecB);
	float DotProduct(Vector3 vecB);
	bool IsEqual(Vector3 vecB);

	Vector3 operator * (float scalar);
	Vector3& operator =(const Vector3& vecOrig);
	bool operator == (Vector3 vecB);
	float operator * (Vector3 vecB);
	float Alpha();

	void ToString();

	float mX, mY, mZ;
};

#endif /* Vector3_h */

