#pragma once

#include <iostream>
#include <vector>

class Point3D
{
public:
	Point3D();
	Point3D(float fX, float fY, float fZ);

	Point3D operator * (float fScalar);
	Point3D operator + (float fScalar);
	Point3D operator + (Point3D pntScalar);
	Point3D operator / (float fScalar);

	void ToString();

	float m_fVertexX, m_fVertexY, m_fVertexZ;

};

class Point3DVector : public std::vector<Point3D>
{

};