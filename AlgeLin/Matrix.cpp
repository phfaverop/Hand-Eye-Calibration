//
//  Matrix.cpp
//  Hand_Eye_Calibration
//
//  Created by Paulo Favero Pereira on 9/4/19.
//  Copyright � 2019 Paulo Favero Pereira. All rights reserved.
//

#include "Matrix.h"

///////////////////////////////////////////////////////////////////////////
///////////////                Matrix Class                 ///////////////
///////////////////////////////////////////////////////////////////////////


Matrix3x3::Matrix3x3()
{
	m11 = 1.0; m12 = 0.0; m13 = 0.0;
	m21 = 0.0; m22 = 1.0; m23 = 0.0;
	m31 = 0.0; m32 = 0.0; m33 = 1.0;
}


Matrix3x3::Matrix3x3(float v11, float v12, float v13,
	float v21, float v22, float v23,
	float v31, float v32, float v33)
{
	m11 = v11; m12 = v12; m13 = v13;
	m21 = v21; m22 = v22; m23 = v23;
	m31 = v31; m32 = v32; m33 = v33;
}


Matrix3x3::Matrix3x3(Vector3 rotAxis, float angle)
{
	float versTheta = 1 - cos(angle);
	//    m11 = pow(rotAxis.mX, 2) + (1 - pow(rotAxis.mX,2)) * cos(angle);
	//    m12 = (rotAxis.mX * rotAxis.mY) * versTheta - rotAxis.mZ * sin(angle);
	//    m13 = (rotAxis.mX * rotAxis.mZ) * versTheta + rotAxis.mY * sin(angle);
	//
	//    m21 = (rotAxis.mX * rotAxis.mY) * versTheta + rotAxis.mZ * sin(angle);
	//    m22 = pow(rotAxis.mY, 2) + (1 - pow(rotAxis.mY,2)) * cos(angle);
	//    m23 = (rotAxis.mY * rotAxis.mZ) * versTheta - rotAxis.mX * sin(angle);
	//
	//    m31 = (rotAxis.mX * rotAxis.mZ) * versTheta + rotAxis.mY * sin(angle);
	//    m32 = (rotAxis.mY * rotAxis.mZ) * versTheta + rotAxis.mX * sin(angle);
	//    m33 = pow(rotAxis.mZ, 2) + (1 - pow(rotAxis.mZ, 2)) * cos(angle);

	m11 = pow(rotAxis.mX, 2) * versTheta + cos(angle);
	m12 = rotAxis.mX * rotAxis.mY * versTheta - rotAxis.mZ * sin(angle);
	m13 = rotAxis.mX * rotAxis.mZ * versTheta + rotAxis.mY * sin(angle);

	m21 = rotAxis.mX * rotAxis.mY * versTheta + rotAxis.mZ * sin(angle);
	m22 = pow(rotAxis.mY, 2) * versTheta + cos(angle);
	m23 = rotAxis.mY * rotAxis.mZ * versTheta - rotAxis.mX * sin(angle);

	m31 = rotAxis.mX * rotAxis.mZ * versTheta - rotAxis.mY * sin(angle);
	m32 = rotAxis.mY * rotAxis.mZ * versTheta + rotAxis.mX * sin(angle);
	m33 = pow(rotAxis.mZ, 2) * versTheta + cos(angle);

}

//
//Matrix::Matrix (Vector3 vecPr)
//{
//    Matrix matRes;
//    Matrix matIdent = Matrix ();
//    float alpha = vecPr.Alpha();
//    float res = 1.0 - (vecPr.Magnitude() / 2.0);
//    matIdent.MultScalar(res);
//    float vecProd = (vecPr.MultVector(vecPr));
//    Matrix matSkew = Skew(vecPr);
//    if(!matSkew.IsSimetricSkew())
//        std::runtime_error("Not Skew Matrix");
//
//    matSkew.MultScalar(alpha);
//    matSkew = (matSkew + vecProd);
//    matSkew.MultScalar(0.5);
//    matRes = matIdent + matSkew;
//
//    m11 = matRes.m11; m12 = matRes.m12; m13 = matRes.m13;
//    m21 = matRes.m21; m22 = matRes.m22; m23 = matRes.m23;
//    m31 = matRes.m31; m32 = matRes.m32; m33 = matRes.m33;
//}


Matrix3x3 Matrix3x3::operator * (float scalar)
{
	Matrix3x3 matRes;
	matRes.m11 = m11 * scalar; matRes.m12 = m12 * scalar; matRes.m13 = m13 * scalar;
	matRes.m21 = m21 * scalar; matRes.m22 = m22 * scalar; matRes.m23 = m23 * scalar;
	matRes.m31 = m31 * scalar; matRes.m32 = m32 * scalar; matRes.m33 = m33 * scalar;
	return matRes;
}

Matrix3x3 Matrix3x3::operator * (Matrix3x3 matB)
{
	Matrix3x3 matRes;
	matRes.m11 = m11 * matB.m11; matRes.m12 = m12 * matB.m21; matRes.m13 = m13 * matB.m31;
	matRes.m21 = m21 * matB.m12; matRes.m22 = m22 * matB.m22; matRes.m23 = m23 * matB.m32;
	matRes.m31 = m31 * matB.m13; matRes.m32 = m32 * matB.m23; matRes.m33 = m33 * matB.m33;
	return matRes;
}


void Matrix3x3::operator = (Matrix3x3 matB)
{
	m11 = matB.m11; m12 = matB.m12; m13 = matB.m13;
	m21 = matB.m21; m22 = matB.m22; m23 = matB.m23;
	m31 = matB.m31; m32 = matB.m32; m33 = matB.m33;
}


Matrix3x3 Matrix3x3::operator + (Matrix3x3 matB)
{
	Matrix3x3 matRes;
	matRes.m11 = m11 + matB.m11; matRes.m12 = m12 + matB.m12; matRes.m13 = m13 + matB.m13;
	matRes.m21 = m21 + matB.m21; matRes.m22 = m22 + matB.m22; matRes.m23 = m23 + matB.m23;
	matRes.m31 = m31 + matB.m31; matRes.m32 = m32 + matB.m32; matRes.m33 = m33 + matB.m33;

	return matRes;
}


Matrix3x3 Matrix3x3::operator + (float scalar)
{
	Matrix3x3 matRes;
	matRes.m11 = m11 + scalar; matRes.m12 = m12 + scalar; matRes.m13 = m13 + scalar;
	matRes.m21 = m21 + scalar; matRes.m22 = m22 + scalar; matRes.m23 = m23 + scalar;
	matRes.m31 = m31 + scalar; matRes.m32 = m32 + scalar; matRes.m33 = m33 + scalar;

	return matRes;
}


Matrix3x3 Matrix3x3::operator - (Matrix3x3 matB)
{
	Matrix3x3 matRes;
	matRes.m11 = m11 - matB.m11; matRes.m12 = m12 - matB.m12; matRes.m13 = m13 - matB.m13;
	matRes.m21 = m21 - matB.m21; matRes.m22 = m22 - matB.m22; matRes.m23 = m23 - matB.m23;
	matRes.m31 = m31 - matB.m31; matRes.m32 = m32 - matB.m32; matRes.m33 = m33 - matB.m33;

	return matRes;
}



void Matrix3x3::MultScalar(float scalar)
{
	m11 *= scalar; m12 *= scalar; m13 *= scalar;
	m21 *= scalar; m22 *= scalar; m23 *= scalar;
	m31 *= scalar; m32 *= scalar; m33 *= scalar;
}


Vector3 Matrix3x3::MultFront(Vector3 vec)
{
	Vector3 vecRes;

	vecRes.mX = m11 * vec.mX + m12 * vec.mY + m13 * vec.mZ;
	vecRes.mY = m21 * vec.mX + m22 * vec.mY + m23 * vec.mZ;
	vecRes.mZ = m31 * vec.mX + m32 * vec.mY + m33 * vec.mZ;

	return vecRes;
}


Vector3 Matrix3x3::MultBack(Vector3 vec)
{
	Vector3 vecRes;

	vecRes.mX = m11 * vec.mX + m21 * vec.mY + m31 * vec.mZ;
	vecRes.mY = m12 * vec.mX + m22 * vec.mY + m32 * vec.mZ;
	vecRes.mZ = m13 * vec.mX + m23 * vec.mY + m33 * vec.mZ;

	return vecRes;
}

void Matrix3x3::Transpose()
{
	Matrix3x3 matInter;
	matInter = GetCopy();
	//    matInter.m11 = m11; matInter.m12 = m12; matInter.m13 = m13;
	//    matInter.m21 = m21; matInter.m22 = m22; matInter.m23 = m23;
	//    matInter.m31 = m31; matInter.m32 = m32; matInter.m33 = m33;

	m11 = matInter.m11; m12 = matInter.m21; m13 = matInter.m31;
	m21 = matInter.m12; m22 = matInter.m22; m23 = matInter.m32;
	m31 = matInter.m13; m32 = matInter.m23; m33 = matInter.m33;
}


Matrix3x3 Matrix3x3::GetTranspose()
{
	Matrix3x3 matInter;
	matInter = GetCopy();
	matInter.Transpose();
	return matInter;
}


bool Matrix3x3::Equals(Matrix3x3 matB)
{
	bool bRes;
	matB.Transpose();
	Matrix3x3 matA = GetCopy();
	Matrix3x3 matRes = matA * matB;
	bRes = ((abs(m11 - matRes.m11) <= FLT_EPSILON) && (abs(m12 - matRes.m12) <= FLT_EPSILON) && (abs(m13 - matRes.m13) <= FLT_EPSILON)) &&
		((abs(m21 - matRes.m21) <= FLT_EPSILON) && (abs(m22 - matRes.m22) <= FLT_EPSILON) && (abs(m23 - matRes.m23) <= FLT_EPSILON)) &&
		((abs(m31 - matRes.m31) <= FLT_EPSILON) && (abs(m32 - matRes.m32) <= FLT_EPSILON) && (abs(m33 - matRes.m33) <= FLT_EPSILON));

	return true;
}


bool Matrix3x3::IsIdentity()
{
	if ((m11 - 1.0) > FLT_EPSILON)
		return false;
	if (m12 > FLT_EPSILON)
		return false;
	if (m13 > FLT_EPSILON)
		return false;
	if (m21 > FLT_EPSILON)
		return false;
	if ((m22 - 1.0) > FLT_EPSILON)
		return false;
	if (m23 > FLT_EPSILON)
		return false;
	if (m31 > FLT_EPSILON)
		return false;
	if (m32 > FLT_EPSILON)
		return false;
	if ((m33 - 1.0) > FLT_EPSILON)
		return false;

	return true;
}


void Matrix3x3::SetZero()
{
	m11 = float(); m12 = float(); m13 = float();
	m21 = float(); m22 = float(); m23 = float();
	m31 = float(); m32 = float(); m33 = float();
}


bool Matrix3x3::IsZero()
{
	if (m11 > FLT_EPSILON)
		return false;
	if (m12 > FLT_EPSILON)
		return false;
	if (m13 > FLT_EPSILON)
		return false;
	if (m21 > FLT_EPSILON)
		return false;
	if (m22 > FLT_EPSILON)
		return false;
	if (m23 > FLT_EPSILON)
		return false;
	if (m31 > FLT_EPSILON)
		return false;
	if (m32 > FLT_EPSILON)
		return false;
	if (m33 > FLT_EPSILON)
		return false;

	return true;
}


Matrix3x3 Matrix3x3::GetCopy()
{
	Matrix3x3 matRes;
	matRes.m11 = m11; matRes.m12 = m12; matRes.m13 = m13;
	matRes.m21 = m21; matRes.m22 = m22; matRes.m23 = m23;
	matRes.m31 = m31; matRes.m32 = m32; matRes.m33 = m33;
	return matRes;
}


bool Matrix3x3::IsSimetricSkew()
{
	Matrix3x3 matTransp, matCopy;
	matTransp = GetCopy();
	matCopy = GetCopy();
	matTransp.Transpose();
	return (matTransp + matCopy).IsZero();

	return true;
}


float Matrix3x3::Trace()
{
	float theta;
	theta = acos((m11 + m22 + m33 - 1) / 2);
	return theta;
}


Vector3 Matrix3x3::GetRotationalAxis()
{
	Vector3 axisRes;
	float theta = Trace();

	if (theta < FLT_EPSILON)
		throw std::runtime_error("Math error: Attempted to divide by Zero\n");

	float alpha = 1 / (2 * sin(theta));

	axisRes.mX = alpha * (m32 - m23);
	axisRes.mY = alpha * (m13 - m31);
	axisRes.mZ = alpha * (m21 - m12);

	return axisRes;

}


Matrix3x3 Matrix3x3::Skew(Vector3 rotAxis)
{
	Matrix3x3 matRes;
	matRes.m11 = float(); matRes.m12 = -rotAxis.mZ; matRes.m13 = rotAxis.mY;
	matRes.m21 = rotAxis.mZ; matRes.m22 = float(); matRes.m23 = -rotAxis.mX;
	matRes.m31 = -rotAxis.mY; matRes.m32 = rotAxis.mX; matRes.m33 = float();

	return matRes;
}


void Matrix3x3::ToString()
{
	std::cout << "Matrix3x3 \n( " << m11 << ", " << m12 << ", " << m13 << "," << std::endl;
	std::cout << m21 << ", " << m22 << ", " << m23 << "," << std::endl;
	std::cout << m31 << ", " << m32 << ", " << m33 << " )" << std::endl;
}
