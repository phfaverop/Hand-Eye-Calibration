#pragma once

#include "GLFW/glfw3.h"
#include "glm/glm.hpp"
#include "glm/vec3.hpp"
#include "glm/vec4.hpp"
#include "glm/matrix.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"


//#include "../Dependencies/include/glad/glad.h"
//#include "../Dependencies/glfw-3.3/include/GLFW/glfw3.h"
//#include "../Dependencies/include/glm/glm.hpp"
//#include "../Dependencies/include/glm/vec3.hpp" // glm::vec3
//#include "../Dependencies/include/glm/gtc/matrix_transform.hpp"
//#include "../Dependencies/include/glm/gtc/type_ptr.hpp"
//#include "../Dependencies/include/glm/vec4.hpp" // glm::vec4
//#include "../Dependencies/include/glm/mat4x4.hpp" // glm::mat4
//#include "../Dependencies/include/glm/matrix.hpp" // glm::translate, glm::rotate, glm::scale, glm::perspective