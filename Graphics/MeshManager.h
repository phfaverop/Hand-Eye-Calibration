#pragma once

#ifndef MESH_H
#define MESH_H

#include "Shader.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

struct Vertex {
    // position
    glm::vec3 Position;
    // normal
    glm::vec3 Normal;
    // texCoords
    glm::vec2 TexCoords;
    // tangent
    glm::vec3 Tangent;
    // bitangent
    glm::vec3 Bitangent;
};

struct Texture {
    unsigned int Id;
    std::string Type;
    std::string Path;
};

class MeshManager {
public:
    /*  Mesh Data  */
    std::vector<Vertex> m_vecVertices;
    std::vector<unsigned int> m_vecIndices;
    std::vector<Texture> m_vecTextures;
    unsigned int m_unVAO;


    MeshManager(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures);

    // render the mesh
    void Draw(Shader shader);

private:
    /*  Render data  */
    unsigned int m_unVBO, m_unEBO;

    /*  Functions    */
    // initializes all the buffer objects/arrays
    void SetupMesh();
};

#endif MESH__H