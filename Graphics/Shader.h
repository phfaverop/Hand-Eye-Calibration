#pragma once

#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "glm/glm.hpp"
#include "glm/vec3.hpp"
#include "glm/vec4.hpp"
#include "glm/matrix.hpp"
#include "AlgeLin.h"
#include <string>
#include <vector>
#include <iostream>

class Shader {
public:
	Shader();
	~Shader();

	// activate the shader
	// ------------------------------------------------------------------------
	void Start();
	// utility uniform functions
	// ------------------------------------------------------------------------
	void SetBool(const std::string& name, bool value) const;
	void SetInt(const std::string& name, int value) const;
	void SetFloat(const std::string& name, float value) const;
	void SetVec2(const std::string& name, const glm::vec2& value) const;
	void SetVec2(const std::string& name, float x, float y) const;
	void SetVec3(const std::string& name, const Vector3 vecOff) const;
	void SetVec3(const std::string& name, float x, float y, float z) const;
	void SetVec4(const std::string& name, const glm::vec4& value) const;
	void SetVec4(const std::string& name, float x, float y, float z, float w) const;
	void SetMat2(const std::string& name, const glm::mat2& mat) const;
	void SetMat3(const std::string& name, const Matrix3x3 mat) const;
	void SetMat4(const std::string& name, const Pose34 pose) const;
	void SetMat4(const std::string& name, const glm::mat4& mat) const;

	int m_nShaderProgramID;

protected:
	void CreateVertexShader();
	void CreateFragmentShader();
	std::vector<int> m_vecShaders;
	void LinkShaders();

private:
	const char* m_shaderVertexSource = "#version 330 core\n"
		"layout(location = 0) in vec3 aPos; \n"
		"layout(location = 1) in vec3 aNormal; \n"
		"layout(location = 2) in vec2 aTexCoords; \n"

		"out vec2 TexCoords; \n"

		"uniform mat4 model; \n"
		"uniform mat4 view; \n"
		"uniform mat4 projection; \n"

		"void main()\n"
		"{\n"
		"TexCoords = aTexCoords; \n"
		"gl_Position = projection * view * model * vec4(aPos, 1.0); \n"
		"}\n";

	const char* m_shaderFragmentSource = "#version 330 core\n"
		"out vec4 FragColor;\n"
		"in vec2 TexCoords;\n"
		"uniform sampler2D texture_diffuse1;\n"
		"void main()\n"
		"{\n"
		"   FragColor = texture(texture_diffuse1, TexCoords);\n"
		"}\n";

	const char* m_shaderLight = "#version 330 core\n"
		"layout(location = 0) in vec3 aPos;\n"

		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"

		"void main()\n"
		"{\n"
		"gl_Position = projection * view * model * vec4(aPos, 1.0); \n"
		"}";

	const char* m_shaderFragColor = "#version 330 core\n"
		"out vec4 FragColor; \n"

		"uniform vec3 objectColor; \n"
		"uniform vec3 lightColor; \n"

		"void main()\n"
		"{\n"
		"FragColor = vec4(lightColor * objectColor, 1.0); \n"
		"}\n";

	// Utility function for checking shader compilation/linking errors.
	// ------------------------------------------------------------------------
	void CheckCompileErrors(unsigned int shader, std::string type);
};