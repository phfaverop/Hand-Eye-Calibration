#include "Renderer.h"
#include <filesystem>


Renderer::Renderer(int argc, char** argv, const char* pTitle, bool bWithDepth, bool bWithStencil)
{
	GLFWContextInit(argc, argv, bWithDepth, bWithStencil);
	if (GLFWBackendCreateWindow(false, pTitle))
	{

	}
	m_shaderManager = new Shader();
	m_camera = Camera(glm::vec3(0.0f, 0.0f, 3.0f));
	std::string path = "../Resources/Models/nanosuit/nanosuit.obj";
	ModelImporter modelTest(path);
	m_vecModelBuffer.push_back(modelTest);
}

Renderer::~Renderer()
{
	glfwDestroyWindow(m_pGLWindow);
	delete m_shaderManager;
}

uint16_t Renderer::GetWindowWidth()
{
	return m_nWidth;
}

uint16_t Renderer::GetWindowHeight()
{
	return m_nHeigth;
}

void Renderer::GLFWContextInit(int argc, char** argv, bool WithDepth, bool WithStencil)
{
	//Error Callback should be set before glfwInit
	//glfwSetErrorCallback(ErrorCalback);

	if (glfwInit() != 1) {
		//OGLDEV_ERROR("Error initializing GLFW");
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		std::cout << "Error initializing GLFW" << std::endl;
		exit(1);
	}

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

	int Major, Minor, Rev;
	glfwGetVersion(&Major, &Minor, &Rev);
	std::cout << "GLFW " << Major << "." << Minor << "." << Rev << " initialized" << std::endl;

	//Initialize Glad
}

bool Renderer::GLFWBackendCreateWindow(bool isFullScreen, const char* pTitle)
{
	m_pGLMonitor = isFullScreen ? glfwGetPrimaryMonitor() : NULL;
	m_pGLWindow = glfwCreateWindow(m_nWidth, m_nHeigth, pTitle, m_pGLMonitor, NULL);
	glfwSetWindowUserPointer(m_pGLWindow, this);
	if (!m_pGLWindow) {
		glfwTerminate();
		//ErrorCalback (1, "Window creation failed");
		exit(1);
	}

	//make the window current on the calling thread
	glfwMakeContextCurrent(m_pGLWindow);
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		exit(1);
	}
	InitCallbacks(m_pGLWindow);
	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);

	// tell GLFW to capture our mouse
	glfwSetInputMode(m_pGLWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	return (m_pGLWindow != NULL);
}

void Renderer::Run()
{
	if (m_pGLWindow != NULL)
	{
		while (!glfwWindowShouldClose(m_pGLWindow))
		{
			double currentFrame = glfwGetTime();
			m_dDeltaTime = currentFrame - m_dLastFrame;
			m_dLastFrame = currentFrame;

			float ratio;
			int width, height;

			glfwGetFramebufferSize(m_pGLWindow, &width, &height);
			ratio = width / (float)height;

			/*The first two parameters of glViewport set the location of the lower left corner of the window.
			The third and fourth parameter set the width and height of the rendering window in pixels, which
			we set equal to GLFW's window size.*/
			glViewport(0, 0, m_nWidth, m_nHeigth);

			// render
			glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			m_shaderManager->Start();

			// view/projection transformations
			glm::mat4 projection = glm::perspective(glm::radians(m_camera.m_fZoom), (float)width / (float)height, 0.1f, 100.0f);
			glm::mat4 view = m_camera.GetViewMatrix();
			m_shaderManager->SetMat4("projection", projection);
			m_shaderManager->SetMat4("view", view);

			// render the loaded model
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(0.0f, -1.75f, 0.0f)); // translate it down so it's at the center of the scene
			model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));	// it's a bit too big for our scene, so scale it down
			m_shaderManager->SetMat4("model", model);
			m_vecModelBuffer.at(0).Draw(*m_shaderManager);

			// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
			// -------------------------------------------------------------------------------
			glfwSwapBuffers(m_pGLWindow);
			glfwPollEvents();
		}

		glfwDestroyWindow(m_pGLWindow);

		glfwTerminate();
		m_shaderManager->~Shader();
		exit(EXIT_SUCCESS);

	}
}

double Renderer::GetElapsedTime()
{
	return glfwGetTime();
}

/*GLFW windows always use double-buffering. That means that you have two rendering buffers;
a front buffer and a back buffer. The front buffer is the one being displayed and the back buffer
the one you render to. When the entire frame has been rendered, it is time to swap the backand
the front buffers in order to display the rendered frame, and begin rendering a new frame*/
void Renderer::SwapBuffer(GLFWwindow* pWindow)
{
	if (pWindow)
	{
		glfwSwapBuffers(pWindow);
	}
}

void ErrorCalback(int error, const char* description)
{
	fputs(description, stderr);
}

void Renderer::KeyCallback(GLFWwindow* pWindow, int key, int scancode, int action, int mods)
{
	Renderer* obj = (Renderer*)glfwGetWindowUserPointer(pWindow);

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(pWindow, GL_TRUE);
	}
	if (key == GLFW_KEY_W && action == GLFW_PRESS)
	{
		GLint polygonMode;
		glGetIntegerv(GL_POLYGON_MODE, &polygonMode);
		if (polygonMode == GL_LINE) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}
		else if (polygonMode == GL_FILL) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		}

	}
	if (glfwGetKey(pWindow, GLFW_KEY_UP) == GLFW_PRESS)
		obj->m_camera.ProcessKeyboard(Camera_Movement::FORWARD, obj->m_dDeltaTime);
	if (glfwGetKey(pWindow, GLFW_KEY_DOWN) == GLFW_PRESS)
		obj->m_camera.ProcessKeyboard(Camera_Movement::BACKWARD, obj->m_dDeltaTime);
	if (glfwGetKey(pWindow, GLFW_KEY_LEFT) == GLFW_PRESS)
		obj->m_camera.ProcessKeyboard(Camera_Movement::LEFT, obj->m_dDeltaTime);
	if (glfwGetKey(pWindow, GLFW_KEY_RIGHT) == GLFW_PRESS)
		obj->m_camera.ProcessKeyboard(Camera_Movement::RIGHT, obj->m_dDeltaTime);
}

void Renderer::CursorPosCallback(GLFWwindow* pWindow, double xPos, double yPos)
{
	Renderer* obj = (Renderer*)glfwGetWindowUserPointer(pWindow);
	if (obj->m_bFirstMouse)
	{
		obj->m_fLastXPos = xPos;
		obj->m_fLastYPos = yPos;
		obj->m_bFirstMouse = false;
	}

	float xOffset = xPos - obj->m_fLastXPos;
	float yOffset = obj->m_fLastYPos - yPos; // reversed since y-coordinates go from bottom to top

	obj->m_fLastXPos = xPos;
	obj->m_fLastYPos = yPos;

	obj->m_camera.ProcessMouseMovement(xOffset, yOffset);
}

void Renderer::MouseCallback(GLFWwindow* pWindow, int Button, int Action, int Mode)
{
	Renderer* obj = (Renderer*)glfwGetWindowUserPointer(pWindow);

}

void Renderer::ScrollCallback(GLFWwindow* pWindow, double xoffset, double yoffset)
{
	Renderer* obj = (Renderer*)glfwGetWindowUserPointer(pWindow);
	obj->m_camera.ProcessMouseScroll(yoffset);
}

void Renderer::framebuffer_size_callback(GLFWwindow* pWindow, int width, int height)
{
	glViewport(0, 0, width, height);
}

void Renderer::InitCallbacks(GLFWwindow* pWindow)
{
	if (pWindow != NULL)
	{
		glfwSetKeyCallback(pWindow, KeyCallback);
		glfwSetCursorPosCallback(pWindow, CursorPosCallback);
		glfwSetMouseButtonCallback(pWindow, MouseCallback);
		glfwSetScrollCallback(pWindow, ScrollCallback);
		glfwSetFramebufferSizeCallback(pWindow, framebuffer_size_callback);
	}
}

void Renderer::FrameBufferCallBack(GLFWwindow* window, int width, int height)
{
	m_nHeigth = height;
	m_nWidth = width;
	glViewport(0, 0, width, height);
}


