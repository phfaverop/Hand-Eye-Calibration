#pragma once

#include "Shader.h"
#include "Camera.h"
#include "ModelImporter.h"
#include <cstdio>
#include <thread>
#include <iostream>
#include <fstream>
#include <sstream>

class Renderer {
public:
	GLFWwindow* m_pGLWindow;
	GLFWmonitor* m_pGLMonitor; //'GLFWmonitor' is an opaque GLFW object that represents the physical monitor.
	unsigned int m_nVBO; //vertex buffer objects
	unsigned int m_nVAO;
	unsigned int m_nEBO; //element buffer objects
	unsigned int m_nLightVAO;

	Renderer(int argc, char** argv, const char* pTitle, bool bWithDepth = true, bool bWithStencil = true);
	~Renderer();
	void Run();
	double GetElapsedTime();

	//Callbacks
	static void KeyCallback(GLFWwindow* pWindow, int key, int scancode, int action, int mods);
	static void CursorPosCallback(GLFWwindow* pWindow, double xPose, double yPos);
	static void MouseCallback(GLFWwindow* pWindow, int Button, int Action, int Mode);
	static void ScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
	static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
	static void InitCallbacks(GLFWwindow* pGLWindow);
	void FrameBufferCallBack(GLFWwindow* window, int width, int height);

protected:
	std::vector<ModelImporter> m_vecModelBuffer;

private:
	uint16_t m_nWidth = 1200;
	uint16_t m_nHeigth = 720;
	Shader* m_shaderManager;
	Camera m_camera;
	//Timing
	double m_dLastFrame;
	double m_dDeltaTime;

	//Mouse
	bool m_bFirstMouse = true;
	//Previous mouse position
	float m_fLastXPos = m_nWidth / 2.0f;
	float m_fLastYPos = m_nHeigth / 2.0f;

	uint16_t GetWindowWidth();
	uint16_t GetWindowHeight();
	void GLFWContextInit(int argc, char** argv, bool WithDepth, bool WithStencil);
	bool GLFWBackendCreateWindow(bool isFullScreen, const char* pTitle);
	void SwapBuffer(GLFWwindow* pWindow);
};
