#include "Shader.h"

Shader::Shader()
{
	////
	CreateVertexShader();
	CreateFragmentShader();
	LinkShaders();
}

Shader::~Shader()
{
	////
}

// build and compile our shader program
void Shader::CreateVertexShader()
{
	// vertex shader
	int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &m_shaderVertexSource, NULL);
	glCompileShader(vertexShader);
	// check for shader compile errors
	CheckCompileErrors(vertexShader, "VERTEX");
	m_vecShaders.push_back(vertexShader);
}

void Shader::CreateFragmentShader()
{
	// fragment shader
	int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &m_shaderFragmentSource, NULL);
	glCompileShader(fragmentShader);
	// check for shader compile errors
	CheckCompileErrors(fragmentShader, "FRAGMENT");
	m_vecShaders.push_back(fragmentShader);
}

void Shader::LinkShaders()
{
	m_nShaderProgramID = glCreateProgram();
	for (unsigned int i = 0; i < m_vecShaders.size(); i++)
	{
		glAttachShader(m_nShaderProgramID, m_vecShaders.at(i));
	}
	glLinkProgram(m_nShaderProgramID);
	// check for linking errors
	CheckCompileErrors(m_nShaderProgramID, "PROGRAM");
	for (int i = 0; i < m_vecShaders.size(); i++)
	{
		glDeleteShader(m_vecShaders.at(i));
	}
}
//TODO: Throw error if fails
void Shader::CheckCompileErrors(unsigned int shader, std::string type)
{
	int success;
	char infoLog[1024];
	if (type != "PROGRAM")
	{
		glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(shader, 1024, NULL, infoLog);
			std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
		}
	}
	else
	{
		glGetProgramiv(shader, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(shader, 1024, NULL, infoLog);
			std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
		}
	}
}

void Shader::Start()
{
	glUseProgram(m_nShaderProgramID);
}

void Shader::SetBool(const std::string& name, bool value) const
{
	glUniform1i(glGetUniformLocation(m_nShaderProgramID, name.c_str()), (int)value);
}

void Shader::SetInt(const std::string& name, int value) const
{
	glUniform1i(glGetUniformLocation(m_nShaderProgramID, name.c_str()), value);
}

void Shader::SetFloat(const std::string& name, float value) const
{
	glUniform1f(glGetUniformLocation(m_nShaderProgramID, name.c_str()), value);
}

// ------------------------------------------------------------------------
void Shader::SetVec2(const std::string& name, const glm::vec2& value) const
{
	glUniform2fv(glGetUniformLocation(m_nShaderProgramID, name.c_str()), 1, &value[0]);
}
void Shader::SetVec2(const std::string& name, float x, float y) const
{
	glUniform2f(glGetUniformLocation(m_nShaderProgramID, name.c_str()), x, y);
}
// ------------------------------------------------------------------------
void Shader::SetVec3(const std::string& name, const Vector3 vecOff) const
{
	glm::vec3 vec = glm::vec3(vecOff.mX, vecOff.mY, vecOff.mZ);
	glUniform3fv(glGetUniformLocation(m_nShaderProgramID, name.c_str()), 1, &vec[0]);
}
void Shader::SetVec3(const std::string& name, float x, float y, float z) const
{
	glUniform3f(glGetUniformLocation(m_nShaderProgramID, name.c_str()), x, y, z);
}
// ------------------------------------------------------------------------
void Shader::SetVec4(const std::string& name, const glm::vec4& value) const
{
	glUniform4fv(glGetUniformLocation(m_nShaderProgramID, name.c_str()), 1, &value[0]);
}
void Shader::SetVec4(const std::string& name, float x, float y, float z, float w) const
{
	glUniform4f(glGetUniformLocation(m_nShaderProgramID, name.c_str()), x, y, z, w);
}
// ------------------------------------------------------------------------
void Shader::SetMat2(const std::string& name, const glm::mat2& mat) const
{
	glUniformMatrix2fv(glGetUniformLocation(m_nShaderProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
// ------------------------------------------------------------------------

void Shader::SetMat3(const std::string& name, Matrix3x3 orient) const
{
	//OpenGL is column major convention. Matrix3x3 and Pose34 are Row major conventions.
	//GL_TRUE transpose the matrix (column major to row major)
	float vecMat[9] = { orient.m11, orient.m12, orient.m13,
					orient.m21, orient.m22, orient.m23,
					orient.m31, orient.m32, orient.m33 };
	glUniformMatrix3fv(glGetUniformLocation(m_nShaderProgramID, name.c_str()), 1, GL_TRUE, &vecMat[0]);
}
// ------------------------------------------------------------------------
void Shader::SetMat4(const std::string& name, const Pose34 pose) const
{
	float vecPose[16] = { pose.m_matRot.m11, pose.m_matRot.m12, pose.m_matRot.m13, pose.m_vecOff.mX,
					pose.m_matRot.m21, pose.m_matRot.m22, pose.m_matRot.m23, pose.m_vecOff.mY,
					pose.m_matRot.m31, pose.m_matRot.m32, pose.m_matRot.m33, pose.m_vecOff.mZ,
					0.0f, 0.0f, 0.0f, 1.0f };
	glUniformMatrix4fv(glGetUniformLocation(m_nShaderProgramID, name.c_str()), 1, GL_TRUE, &vecPose[0]);
}

void Shader::SetMat4(const std::string& name, const glm::mat4& mat) const
{
	glUniformMatrix4fv(glGetUniformLocation(m_nShaderProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}


