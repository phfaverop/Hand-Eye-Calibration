// HandEye Calibration.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "RobotSim.h"
#include <iostream>

void StartRenderer(int argc, char** argv, const char* title)
{
	Renderer renderer(argc, argv, title);
	renderer.Run();
}

int main(int argc, char** argv)
{
	std::thread m_thdRenderer(StartRenderer, argc, argv, "Robot's Playground");
	m_thdRenderer.join();
	Vector3* teste;
	teste = new Vector3();
	return 0;
}
